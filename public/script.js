mapboxgl.accessToken =
  "pk.eyJ1IjoibWloaXIyMzQiLCJhIjoiY2tvdHdjbXh5MDVreTJ4bXBwd2Q4djI0eCJ9.2UBQTPetNzxrTxr8Dqz79w";

const map = new mapboxgl.Map({
  container: "map", // container ID
  style: "mapbox://styles/mapbox/satellite-v9", // style URL
  center: [-2.81361, 36.77271], // starting position [lng, lat]
  zoom: 13, // starting zoom
});

const layerList = document.getElementById("menu");
const inputs = layerList.getElementsByTagName("input");

for (const input of inputs) {
  input.onclick = (layer) => {
    const layerId = layer.target.id;
    map.setStyle("mapbox://styles/mapbox/" + layerId);
  };
}

map.addControl(
    new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl
    })
);